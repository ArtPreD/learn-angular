import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  consoleText(arg) {
    console.log(arg);
  }

  constructor() { }
}
