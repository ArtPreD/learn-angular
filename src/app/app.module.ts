import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { CarComponent } from './components/main/car.component';
import { ContactsComponent } from './components/directives/contacts.component';
import { FeedbackService } from './services/feedback.service';
import { HttpClientModule } from '@angular/common/http';
import { EventDirective } from './directives/attribute/event/event.directive';
import { ColorDirective } from './directives/attribute/color/color.directive';
import { RepeatDirective } from './directives/attribute/repeat/repeat.directive';
import { ConfirmDirective } from './directives/attribute/confirm/confirm.directive';
import { CoordsDirective } from './directives/attribute/coords/coords.directive';
import { ClickableDirective } from './directives/attribute/clickable/clickable.directive';
import { MyIfDirective } from './directives/structure/if/my-if.directive';
import { MyDelayDirective } from './directives/structure/delay/my-delay.directive';
import { UserCardDirective } from './directives/structure/context/user-card.directive';
import { BuiltinComponent } from './components/pipes/builtin.component';
import { CustomPipePipe } from './pipes/custom/custom-pipe.pipe';
import { FormatPipePipe } from './pipes/format/format-pipe.pipe';
import { PurePipePipe } from './pipes/pure/pure-pipe.pipe';
import { ImpurePipePipe } from './pipes/pure/pure-pipe.pipe';
import { DrivenFormsComponent } from './components/driven-forms/driven-forms.component';
import { DynamicFormsComponent } from './components/dynamic-forms/dynamic-forms.component';
import { DynamicFormsElementComponent } from './components/dynamic-forms/dynamic-forms-element/dynamic-forms-element.component';


const appRoutes: Routes = [
  {path: '', component: CarComponent},
  {path: 'directives', component: ContactsComponent},
  {path: 'pipes', component: BuiltinComponent},
  {path: 'driven', component: DrivenFormsComponent},
  {path: 'dynamic', component: DynamicFormsComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    CarComponent,
    ContactsComponent,
    EventDirective,
    ColorDirective,
    RepeatDirective,
    ConfirmDirective,
    CoordsDirective,
    ClickableDirective,
    MyIfDirective,
    MyDelayDirective,
    UserCardDirective,
    BuiltinComponent,
    CustomPipePipe,
    FormatPipePipe,
    PurePipePipe,
    ImpurePipePipe,
    DrivenFormsComponent,
    DynamicFormsComponent,
    DynamicFormsElementComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [FeedbackService],
  bootstrap: [AppComponent]
})
export class AppModule { }
