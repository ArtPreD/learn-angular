import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'purePipe',
  pure: true
})
export class PurePipePipe implements PipeTransform {

  transform(value: any) {
    return value.firstName + ' ' + value.lastName;
  }

}

@Pipe({
  name: 'impurePipe',
  pure: false
})
export class ImpurePipePipe implements PipeTransform {

  transform(value: any) {
    return value.firstName + ' ' + value.lastName;
  }
}
