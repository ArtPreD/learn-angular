import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'custom'
})
export class CustomPipePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value) {
      return '[' + value + ']';
    } else {
      return '';
    }
  }
}
