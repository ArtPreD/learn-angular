import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[coords]'
})
export class CoordsDirective {

  constructor(private element: ElementRef) { }

  @HostListener('mousemove', ['$event']) onMouseMove(event: MouseEvent) {
    this.element.nativeElement.innerHTML = 'X ' + event.offsetX + ' Y ' + event.offsetY;
  }
}
