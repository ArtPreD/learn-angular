import {Directive, ElementRef, Input, OnInit, Renderer2} from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[repeat], repeat'
})
export class RepeatDirective implements OnInit {

  @Input() message: string;
  @Input() count: number;

  constructor(private elementRef: ElementRef, private render: Renderer2) {
  }

  ngOnInit() {
    for (let i = 0; i < this.count; i++) {
      const elem: HTMLDivElement = this.render.createElement('div');
      this.render.appendChild(this.elementRef.nativeElement, elem);
      elem.innerHTML = this.message;
    }
  }
}
