import {Directive, HostListener, Input} from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[confirm]'
})
export class ConfirmDirective {

  @Input('confirm')
  onConfirmed: () => void;
  @Input() message: string;

  @HostListener('click') onConfirm(event: Event) {
    const confirmed = window.confirm(this.message);
    if (confirmed) {
      this.onConfirmed();
    }
  }
}
