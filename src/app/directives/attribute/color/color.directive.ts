import {Directive, ElementRef, HostListener, Input, Renderer2} from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[changeColor]'
})
export class ColorDirective {

  constructor(private elementRef: ElementRef, private render: Renderer2) { }


  @Input('changeColor') set changeColor(color: string) {
    this.render.setStyle(this.elementRef.nativeElement, 'color', color);
  }
}
