import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[changeColorEvent]'
})
export class EventDirective {

  constructor(private elementRef: ElementRef, private render: Renderer2) { }

  @HostListener('mouseenter') onMouseEnter() {
      this.changeColor('red');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.changeColor(null);
  }

  private changeColor(color: string) {
    this.render.setStyle(this.elementRef.nativeElement, 'color', color);
  }
}
