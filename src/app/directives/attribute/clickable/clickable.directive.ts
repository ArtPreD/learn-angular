import {Directive, ElementRef, HostBinding, HostListener} from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[clickable]'
})
export class ClickableDirective {

  constructor(private element: ElementRef) {
    element.nativeElement.style.cursor = 'pointer';
  }

  @HostBinding('class.pressed') isClicked: boolean;

  @HostListener('mousedown') onMouseDown() {
    this.isClicked = true;
  }

  @HostListener('mouseup') onMouseUp() {
    this.isClicked = false;
  }
}
