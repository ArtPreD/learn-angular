import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[myIf]'
})
export class MyIfDirective {

  constructor(private template: TemplateRef<any>, private viewContainer: ViewContainerRef) {
  }

  @Input('myIf') set MyIfValue(condition: boolean) {
    if (condition) {
      this.viewContainer.createEmbeddedView(this.template);
    } else {
      this.viewContainer.clear();
    }
  }
}
