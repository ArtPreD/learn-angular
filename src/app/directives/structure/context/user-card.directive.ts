import {Directive, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[userCard]'
})
export class UserCardDirective implements OnInit {

  constructor(private template: TemplateRef<any>, private viewContainer: ViewContainerRef) {}

  ngOnInit() {
    const context: UserCardContext = new UserCardContext('Vasya', 'Pupkin');
    this.viewContainer.createEmbeddedView(this.template, context);
  }
}

class UserCardContext {

  public firstName: string;
  public lastName: string;

  constructor(firstName: string, lastName: string) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

}
