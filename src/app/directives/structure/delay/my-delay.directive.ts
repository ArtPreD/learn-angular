import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[myDelay]'
})
export class MyDelayDirective {

  constructor(private template: TemplateRef<any>, private viewContainer: ViewContainerRef) {}

  @Input('myDelay') set delayTime(time: number) {
    setTimeout(() => {
      this.viewContainer.createEmbeddedView(this.template);
    }, time);
  }
}
