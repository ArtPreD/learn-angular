import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  name: string;
  speed: number;
  model: string;
  colors: Colors;
  options: string[];
  isEdit: boolean;

  constructor() {
    this.isEdit  = false;
  }

  ngOnInit() {
    this.name = 'Audi';
    this.speed = 235;
    this.model = 'RS8';
    this.colors = {
      car: 'Белый',
      salon: 'Черный',
      wheels: 'Серебристый'
    };
    this.options = ['ABS', 'Автопилот', 'Автопаркинг'];
  }

  showEdit() {
    this.isEdit = !this.isEdit;
  }

  carSelect(carName) {
    if (carName === 'BMW') {
      this.name = 'BMW';
      this.speed = 280;
      this.model = 'M5';
      this.colors = {
        car: 'Синий',
        salon: 'Белый',
        wheels: 'Серебристый'
      };
      this.options = ['ABS', 'Автопилот', 'Автопаркинг'];
    } else if (carName === 'Audi') {
      this.name = 'Audi';
      this.speed = 235;
      this.model = 'RS8';
      this.colors = {
        car: 'Белый',
        salon: 'Черный',
        wheels: 'Серебристый'
      };
      this.options = ['ABS', 'Автопилот', 'Автопаркинг'];
    } else if (carName === 'Mercedes') {
      this.name = 'Mercedes';
      this.speed = 300;
      this.model = 'MX7';
      this.colors = {
        car: 'Красный',
        salon: 'Бежевый',
        wheels: 'Черный'
      };
      this.options = ['ABS', 'Автопаркинг'];
    }
  }

  addOption(option) {
    this.options.unshift(option);
    return false;
  }

  deleteOption(index) {
    this.options.splice(index, 1);
  }
}

interface Colors {
  car: string;
  salon: string;
  wheels: string;
}
