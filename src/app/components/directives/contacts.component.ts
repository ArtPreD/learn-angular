import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FeedbackService} from '../../services/feedback.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {

  userName: string;
  response: any;
  visible: boolean;

  constructor(private svc: FeedbackService, private http: HttpClient) { }

  ngOnInit() {
  }

  search() {
    this.http.get('https://api.github.com/users/' + this.userName)
      .subscribe((response) => {
        this.response = response;
        this.svc.consoleText(this.userName);
        this.svc.consoleText(response);
      });
  }

  onDelete() {
    console.log('Deleted!!');
  }

  changeVisibility() {
    this.visible = !this.visible;
  }
}
