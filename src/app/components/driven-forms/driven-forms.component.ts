import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { asyncEmailValidator } from './validator/async-email-validatro';
import { BlacklistService } from './service/blacklist.service';
import { blackListValidator } from './validator/black-list-validator';

@Component({
  selector: 'app-forms',
  templateUrl: './driven-forms.component.html',
  styleUrls: ['./driven-forms.component.css'],
  providers: [BlacklistService]
})
export class DrivenFormsComponent implements OnInit {

  registrationForm: FormGroup;
  firstName: FormControl;
  lastName: FormControl;
  email: FormControl;
  password: FormControl;
  confirm: FormControl;

  constructor(private blackListService: BlacklistService) {
  }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
  }

  createFormControls() {
    this.firstName = new FormControl('', Validators.required);
    this.lastName = new FormControl('', Validators.required);
    this.email = new FormControl(
      '', Validators.required, [asyncEmailValidator, blackListValidator(this.blackListService)]);
    this.password = new FormControl(
      '', [Validators.required, Validators.minLength(5)]);
    this.confirm = new FormControl(
      '', [Validators.required, Validators.minLength(5)]);
  }

  createForm() {
    this.registrationForm = new FormGroup({
      name: new FormGroup({
        firstName: this.firstName,
        lastName: this.lastName
      }),
      email: this.email,
      password: this.password,
      confirm: this.confirm
    });
  }

  onSubmit() {
    if (this.registrationForm.valid) {
      console.log('submit!');
      console.log(this.registrationForm.value);
    } else {
    }
  }
}
