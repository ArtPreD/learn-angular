import { Injectable } from '@angular/core';

@Injectable()
export class BlacklistService {

  private list: string[] = [
    'test@gmail.com',
    'admin@test.com',
    'hello@world.info'
  ];

  public getAllBlockedEmails(): string[] {
    return this.list;
  }

  public checkEmail(email: string): Promise<boolean> {
    console.log('start check email ' + email + ' with black list');
    return new Promise(resolve => {
      setTimeout(() => {
        const isBlocked = this.list.find(x => x === email) !== undefined;
        console.log('email is block - ' + isBlocked);
        resolve(isBlocked);
      }, 1000);
    });
  }
}
