import {FormControl} from '@angular/forms';

export function asyncEmailValidator(control: FormControl) {
  return new Promise(resolve => {
    console.log('start validation');
    setTimeout(() => {
      const emailRegex = /[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/i;
      const value = control.value;

      const result = emailRegex.test(value);

      if (result) {
        console.log('email is OK');
        resolve(null);
      } else {
        console.log('validation failed');

        resolve({
          asyncEmailValidator: {
            valid: false
          }
        });
      }
    }, 1000);
  });
}
