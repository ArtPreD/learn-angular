import {FormControl} from '@angular/forms';
import {BlacklistService} from '../service/blacklist.service';

export function blackListValidator(service: BlacklistService) {
  return (control: FormControl) => {
    return new Promise((resolve) => {
      service.checkEmail(control.value).then(
        (response) => {
          if (response) {
            resolve({
              blackListValidator: {
                blocked: true
              }
            });
          } else {
            resolve(null);
          }
        },
        () => {
          console.error('Error loading black list');
        });
    });
  };
}
