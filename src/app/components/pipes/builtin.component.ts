import { Component, OnInit } from '@angular/core';
import {Observable, Subscriber} from 'rxjs';

@Component({
  selector: 'app-builtin',
  templateUrl: './builtin.component.html',
  styleUrls: ['./builtin.component.css']
})
export class BuiltinComponent implements OnInit {

  products: any[];
  creationDate: Date = new Date();

  dataFromServer: Promise<string>;
  resolvePromise: (data: string) => void;
  fail: () => void;

  counter: number;
  time: Observable<number>;

  value: number;

  person1 = {firstName: 'Ivan', lastName: 'Ivanov'};
  person2 = {firstName: 'Ivan', lastName: 'Ivanov'};

  constructor() {
    this.counter = 0;
    this.dataFromServer = new Promise<string>((resolve, reject) => {this.resolvePromise = resolve; this.fail = reject; });
  }

  ngOnInit() {
    this.products = [
      { name: 'Item 1', price: 100, category: 'Category 1', code: 'aa21c', date: new Date(2016, 9, 1)},
      { name: 'Item 2', price: 45, category: 'Category 1', code: 'bc21c', date: new Date(2016, 10, 3)},
      { name: 'Item 3', price: 65.4, category: 'Category 2', code: 'rr57a', date: new Date(2016, 10, 5)},
      { name: 'Item 4', price: 11.8, category: 'Category 2', code: 'dr413', date: new Date(2016, 9, 10)}
    ];
  }

  getData() {
    setTimeout(() => {
      this.resolvePromise('Data from server');
    }, 2000);
  }

  start() {
    this.time = new Observable<number>((observer: Subscriber<number>) => {
      setInterval(() => {
        observer.next(this.counter++);
      }, 1000);
    });
  }

  changePropPerson1() {
    this.person1.lastName = '.....';
  }

  changeRefPerson1() {
    this.person1 = { firstName: '.....', lastName: '.....'};
  }

  changePropPerson2() {
    this.person2.lastName = '.....';
  }

  changeRefPerson2() {
    this.person2 = { firstName: '.....', lastName: '.....'};
  }
}
