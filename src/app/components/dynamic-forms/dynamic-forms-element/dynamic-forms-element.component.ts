import { Component, Input } from '@angular/core';
import { ElementBase } from '../model/element-base';
import { FormGroup } from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dynamic-forms-element',
  templateUrl: './dynamic-forms-element.component.html',
  styleUrls: ['./dynamic-forms-element.component.css']
})
export class DynamicFormsElementComponent {

  @Input()
  public element: ElementBase<any>;
  @Input()
  form: FormGroup;

  constructor() {
  }

  get isValid() {
    return this.form.controls[this.element.key].valid;
  }

  get isDirty() {
    return this.form.controls[this.element.key].dirty;
  }

  get isTouched() {
    return this.form.controls[this.element.key].touched;
  }
}
