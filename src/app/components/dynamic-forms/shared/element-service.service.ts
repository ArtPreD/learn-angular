import { Injectable } from '@angular/core';
import { ElementBase } from '../model/element-base';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Injectable()
export class ElementService {

  constructor() {
  }

  toFormGroup(elements: ElementBase<any>[]) {
    const group: any = {};

    elements.forEach(element => {
      group[element.key] = element.required ? new FormControl(element.value || '', Validators.required)
        : new FormControl(element.value || '');
    });

    return new FormGroup(group);
  }
}
