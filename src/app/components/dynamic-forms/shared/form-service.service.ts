import { Injectable } from '@angular/core';
import { ElementBase } from '../model/element-base';
import { TextboxElement } from '../model/textbox-element';
import { DropdownElement } from '../model/dropdown-element';

@Injectable()
export class FormService {

  getElements() {
    const elements: ElementBase<any>[] = [
      new TextboxElement({
        key: 'firstName',
        label: 'Имя',
        required: true,
        order: 1
      }),
      new TextboxElement({
        key: 'lastName',
        label: 'Фамилия',
        required: true,
        order: 2
      }),
      new TextboxElement({
        key: 'email',
        label: 'Почта',
        required: true,
        order: 3
      }),
      new DropdownElement({
        key: 'language',
        label: 'Выберите язык',
        order: 4,
        options: [
          {key: '1', value: 'Украинский'},
          {key: '2', value: 'Русский'},
          {key: '3', value: 'Английский'}
        ]
      })
    ];

    return elements.sort((a, b) => a.order - b.order);
  }
}
