import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ElementService } from './shared/element-service.service';
import { FormService } from './shared/form-service.service';
import { ElementBase } from './model/element-base';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'form-dynamic',
  templateUrl: './dynamic-forms.component.html',
  styleUrls: ['./dynamic-forms.component.css'],
  providers: [FormService , ElementService]
})
export class DynamicFormsComponent implements OnInit {

  elements: ElementBase<any>[] = [];
  form: FormGroup;

  constructor(private es: ElementService, private fs: FormService) {
  }

  ngOnInit() {
    this.elements = this.fs.getElements();
    this.form = this.es.toFormGroup(this.elements);
  }

  onSubmit() {
    if (this.form.valid) {
      console.log(this.form.value);
    }
  }
}
